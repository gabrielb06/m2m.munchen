const express = require('express')
var app = express()

app.get('/', (req, res) => res.send("Hello, world. Welcome to port 1972"))
app.get('/modlog', (req, res) => res.send("modlog"))

app.listen(1974, () => console.log('Listening on port 1974'))