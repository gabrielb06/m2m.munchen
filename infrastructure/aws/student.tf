# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "gabrielb"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "gabrielbdb"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = "Munich2018!"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1uWYAYEdSr1En7F0faAR8oCe97mWN7/tT1rDVIlwKN2PP97O2ELl6pxs97hYDGG+1c+XkFuoJHEi19J7Ws/k4BWBjYj8sWuwmglv7DBXmyh7phJm3ue19c4np4LKnzwH6998T8AxicMKXbI7qTgTplniGEqLyJ1ry+EaboVXiEzPI3lVwvdsixfvdKkZuGfAQ2MTZQcULjbYI4C/tDtgzYUTPnmKj3gT1UfF674DljUeEZjFwgJBu31biUM9m0ip4RzJ6PmCTTVVxvImiu4jCiHBOlFl1IbNXyrk/XV5NrU/ztKf8ydjHI8M+8xd6TcH4VYt8/vhbhTkRYW/dYDLB FRSXD81GABRIELB+gab@FRSXD81GABRIELB"
}

variable "s3_bucket" {
  description = "This is your personal S3 bucket name"
  default = "gabriel-munich-workshop-bucket"
}

variable "aws_access_key_id" {
  description = "This is the access key ID of an IAM user that can read from your S3 bucket"
  default = "AKIAJFJMWL7EJJJO5EEA"
}

variable "aws_secret_access_key" {
  description = "This is the secret access key of the same IAM user, one who can read from your S3 bucket"
  default = "d1v2t/zkCnSifXkrDe8o6LamMdIuPpTSepRBj3aN"
}
