#cloud-config
write_files:
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/influx.conf
    content: "${influx_config}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - sed -i "s/MY_HOSTNAME/JC_RoutSrv_$HOSTNAME/g" /etc/telegraf/telegraf.d/influx.conf
  - mkdir -p /home/ubuntu/app
  - wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_tarball}" /home/ubuntu/app/
  - sudo dpkg -i telegraf_1.6.3-1_amd64.deb
  - cd /home/ubuntu/app
  - tar xzf /home/ubuntu/app/${router_tarball}
  - LOBSTERS_DNS_NAME=${lobsters_dns_name} MODLOG_DNS_NAME=${modlog_dns_name} node index.js
