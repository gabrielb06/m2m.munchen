require 'hat_request_representer'
require 'faraday'

class HatController < ApplicationController
  before_action :require_logged_in_moderator,
  :except => [ :build_request, :index ]

  def create_request
    @title = "Request a Hat"

  end

  def show
    @title = "Show hats"
  end

end
