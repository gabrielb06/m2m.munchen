package com.n6consulting.m2m.request;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Request {
  @Id
  @GeneratedValue
  private Long requestId;

  private String subject;
  private String object;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "request", fetch = FetchType.EAGER)
  private Set<Evidence> evidence = new HashSet<>();

  public Long getRequestId() {
    return requestId;
  }

  public String getSubject() {
    return subject;
  }

  public String getObject() {
    return object;
  }

  public Set<Evidence> getEvidence() {
    return evidence;
  }

  // Added for 013.request-with-status
  enum Status {
    pending, approved, rejected
  }

  private Status status = Status.pending;

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public boolean isOpen() {
    return status == Status.pending;
  }

  @OneToOne(cascade = CascadeType.ALL)
  private Notarization notarization;

  public Notarization getNotarization() {
    return notarization;
  }

  public Request notarize(Notarization notarization) {
    this.notarization = notarization;
    return this;
  }
}

